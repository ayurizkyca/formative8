package Task2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task2 {
    public static void main(String[] args) {
        String filePath = "Task2/sentence.txt";
        String word = readFile(filePath);

        String changeWithNumber = word.replace("L", "7")
                .replace("i", "1").replace("p", "8")
                .replace("o", "9").replace("s", "5");

        // save number to file day821.txt
        String saveNumber = extractNumber(changeWithNumber);
        saveFile(saveNumber, "day821.txt");

        // save alphabeth to file day822.txt
        String saveAlphabet = extractAlfabeth(changeWithNumber);
        saveFile(saveAlphabet, "day822.txt");

    }

    // Method to read the content of a file
    private static String readFile(String filePath) {
        String content = "";
        try {
           content = new String(Files.readAllBytes(Paths.get(filePath)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }

    //Methode to extract number from sentence
    private static String extractNumber(String input) {
        StringBuilder result = new StringBuilder();
        Pattern pattern = Pattern.compile("\\d+");

        Matcher matcher = pattern.matcher(input);

        while (matcher.find()) {
            result.append(matcher.group() + "\n");
        }
        return result.toString();
    }

    //Methode to extract alphabeth from sentence
    private static String extractAlfabeth(String input) {
        StringBuilder result = new StringBuilder();
        Pattern pattern = Pattern.compile("[a-zA-Z]+");

        Matcher matcher = pattern.matcher(input);

        while (matcher.find()) {
            result.append(matcher.group() + "\n");
        }
        return result.toString();
    }

    //Methode save extracted number and alphabet to txt file
    private static void saveFile(String inputSave, String fileName) {
        String folderPath = "Task2";
        File folder = new File(folderPath);
        try (FileWriter fileWriter = new FileWriter(folder + "/" + fileName)) {
            fileWriter.write(inputSave);
            System.out.println("Generate file-" + fileName + " success");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}