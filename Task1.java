import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Task1 {
    public static void main(String[] args) {
        String filePath = "Task2/sentence.txt";
        String word = readFile(filePath);

        String changeWithNumber = word.replace("L", "7")
                .replace("i", "1").replace("p", "8")
                .replace("o", "9").replace("s", "5");

        System.out.println("====Before Replace===");
        System.out.println(word);
        System.out.println("===After Replace=== ");
        System.out.println(changeWithNumber);
    }

    // Method to read the content of a file
    private static String readFile(String filePath) {
        String content = "";
        try {
            content = new String(Files.readAllBytes(Paths.get(filePath)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }
}
