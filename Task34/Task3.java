package Task34;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        // Get current date
        LocalDate currentDate = LocalDate.now();

        // Get user input for registration day
        LocalDate registrationDate = null;
        boolean newUser = false;

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String registrationDateStr;

        try {
            System.out.print("New user (y/n)? ");
            String inputNewUser = keyboard.nextLine().toLowerCase();
            switch (inputNewUser) {
                case "y":
                    newUser = true;
                    // Input registration day
                    System.out.print("Enter the registration date (dd/MM/yyyy): ");
                    registrationDateStr = keyboard.nextLine();
                    registrationDate = LocalDate.parse(registrationDateStr, dateFormatter);

                    // Validate registration date
                    if (registrationDate.isBefore(currentDate)) {
                        System.out.println("Invalid date. Registration date cannot be in the past.");
                        return;
                    }
                    break;
                case "n":
                    newUser = false;
                    // Input registration day
                    System.out.print("Enter the registration date (dd/MM/yyyy): ");
                    registrationDateStr = keyboard.nextLine();
                    registrationDate = LocalDate.parse(registrationDateStr, dateFormatter);

                    // Validate registration date
                    if (registrationDate.isAfter(currentDate) || registrationDate.isEqual(currentDate)) {
                        System.out.println("Invalid date. Registration date cannot be in the past.");
                        return;
                    }
                    break;
                default:
                    System.out.println("Invalid input. Just input y/n/Y/N");
                    break;
            }
            // Input days reminders
            System.out.print("How many days before payment day do you want to be reminded (1-7)? ");
            int reminderDays = Integer.parseInt(keyboard.nextLine());

            // Create and write to netflix.txt
            writeAndSaveFile(registrationDate, reminderDays, newUser, "NetflixPayment.txt");

        } catch (DateTimeParseException e) {
            System.err.println("Invalid date format.");
        } finally {
            keyboard.close();
        }
    }

    private static void writeAndSaveFile(LocalDate registrationDate, int reminderDays, boolean newUser,
            String fileName) {
        String folderPath = "Task34";
        File folder = new File(folderPath);
        try (FileWriter writer = new FileWriter(folder + "/" + fileName)) {
            LocalDate currentDate = registrationDate;
            boolean registered = false;

            for (int month = 1; month <= 12; month++) {
                if (registered) {
                    // Reminder
                    LocalDate reminderDate = currentDate.minusDays(reminderDays);
                    writer.write(formatDate(reminderDate) + " - Email Reminder\n");
                }

                // Payment day
                writer.write(formatDate(currentDate) + " - payment "
                        + (newUser && (month == 2 || month == 3 || month == 4) ? "(Free)\n" : "(Rp. 153.000)\n"));
                registered = true;

                // Next month
                currentDate = currentDate.plusMonths(1);
            }
            System.out.println("Generate " + fileName + " success");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Method format date
    private static String formatDate(LocalDate date) {
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return dateFormatter.format(date);
    }
}
